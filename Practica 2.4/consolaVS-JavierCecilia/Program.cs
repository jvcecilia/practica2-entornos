﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion = 1;
            Console.WriteLine("Inserta el numero de elementos que quieres que tenga tu array: ");
            int numero = int.Parse(Console.ReadLine());
            int[] array = new int[numero];

            while (opcion != 0)
            {
                Console.WriteLine();
                Console.WriteLine("---------");
                Console.WriteLine("MENU DE ARRAYS DE ENTEROS");
                Console.WriteLine("---------");
                Console.WriteLine("1 - Rellenar el contenido del array manualmente.");
                Console.WriteLine("2 - Visualizar el contenido del array.");
                Console.WriteLine("3 - Modificar el numero de un campo del array.");
                Console.WriteLine("4 - Generar un numero aleatorio en un campo del array.");
                Console.WriteLine("0 - Salir");
                opcion = int.Parse(Console.ReadLine());

                switch (opcion)
                {
                    case 0:
                        Console.WriteLine("Adios! :-)");
                        break;
                    case 1:
                        rellenarArray(array);
                        break;
                    case 2:
                        visualizarArray(array);
                        break;
                    case 3:
                        modificarArray(array);
                        break;
                    case 4:
                        elementoAleatorio(array);
                        break;
                    default:
                        Console.WriteLine("Introduce un número del menu.");
                        break;
                }
            }
        }
        private static void elementoAleatorio(int[] array)
        {
            Random rd = new Random();
            int elemento;
            Console.WriteLine("Que campo del array quieres que genere un numero aleatorio?(0-" + (array.Length - 1) + "): ");
            elemento = int.Parse(System.Console.ReadLine());
            while (elemento < 0 || elemento > (array.Length-1))
            {
                Console.WriteLine("Has introducido un campo no existente. Vuelve a introducirlo: ");
                elemento = int.Parse(System.Console.ReadLine());
            }
            array[elemento] = rd.Next(100);
            Console.WriteLine("El nuevo valor del campo es " + array[elemento]);

        }

        private static void visualizarArray(int[] array)
        {
            Console.WriteLine("------------------------------------");
            Console.WriteLine("Se muestran los elementos de array");
            Console.WriteLine("------------------------------------");
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("Posicion " + i + ": " + array[i]);
            }

        }

        private static void modificarArray(int[] array)
        {
            String element;
            Console.WriteLine("Que campo del array quieres modificar?(0-" + (array.Length - 1) + "): ");
            element = System.Console.ReadLine();
            int elemento = int.Parse(element);
            while (elemento < 0 || elemento >= array.Length)
            {
                Console.WriteLine("Has introducido un campo no existente. Vuelve a introducirlo:");
                elemento = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Inserta el nuevo valor:");
            array[elemento] = int.Parse(Console.ReadLine());
            Console.WriteLine("El nuevo valor del campo es: " + array[elemento]);
        }

        private static void rellenarArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("Introduce el valor del elemento " + i + ":");
                array[i] = int.Parse(Console.ReadLine());
            }

        }
    }
}
