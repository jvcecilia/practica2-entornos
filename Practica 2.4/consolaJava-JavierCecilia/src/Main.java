import java.util.Random;
import java.util.Scanner;
public class Main {

	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		int opcion = 1;
		System.out.print("Inserta el numero de elementos que quieres que tenga tu array: ");
		int numero = Integer.parseInt(sc.nextLine());
		int array[] = new int[numero];
		
		while(opcion!=0){
			System.out.println();
			System.out.println("---------");
			System.out.println("MENU DE ARRAYS DE ENTEROS");
			System.out.println("---------");
			System.out.println("1 - Rellenar el contenido del array manualmente.");
			System.out.println("2 - Visualizar el contenido del array.");
			System.out.println("3 - Modificar el numero de un campo del array.");
			System.out.println("4 - Generar un numero aleatorio en un campo del array.");
			System.out.println("0 - Salir");
			
			opcion = Integer.parseInt(sc.nextLine());
			switch(opcion){
				case 0: System.out.println("Adios! :-)");
				break;
				case 1: rellenarArray(array);
				break;
				case 2: visualizarArray(array);
				break;
				case 3: modificarArray(array);
				break;
				case 4: elementoAleatorio(array);
				break;
				default: System.out.println("Introduce un n�mero del menu.");
				break;
			}
		}
	}

	private static void elementoAleatorio(int[] array) {
		Random rd = new Random();
		System.out.print("Que campo del array quieres que genere un numero aleatorio?(0-" + (array.length-1) + "): ");
		int elemento = Integer.parseInt(sc.nextLine());
		while(elemento<0 || (elemento>array.length-1)){
			System.out.print("Has introducido un campo no existente. Vuelve a introducirlo: ");
			elemento = Integer.parseInt(sc.nextLine());
		}
		array[elemento] = rd.nextInt(100);
		System.out.println("El nuevo valor del campo es " +  array[elemento]);
		
	}

	private static void visualizarArray(int[] array) {
		System.out.println("------------------------------------");
		System.out.println("Se muestran los elementos de array");
		System.out.println("------------------------------------");
		for (int i = 0; i < array.length; i++) {
			System.out.println("Posicion " + i + ": " + array[i]);
		}
		
	}

	private static void modificarArray(int[] array) {
		System.out.print("Que campo del array quieres modificar?(0-" + (array.length-1) + "): ");
		int elemento = Integer.parseInt(sc.nextLine());
		while(elemento<0 || elemento>(array.length-1)){
			System.out.print("Has introducido un campo no existente. Vuelve a introducirlo:");
			elemento = Integer.parseInt(sc.nextLine());
		}
		System.out.print("Inserta el nuevo valor:");
		array[elemento] = Integer.parseInt(sc.nextLine());
		System.out.println("El nuevo valor del campo es: " + array[elemento]);
	}

	private static void rellenarArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print("Introduce el valor del elemento " + i + ":");
			array[i] = Integer.parseInt(sc.nextLine());
		}
		
	}

}
