
public class Main {

	public static void main(String[] args) {
		String cadena = "Buenas noches";
		int numero1 = 6;
		int[] array = new int[3];
		array[0]=1;
		array[1]=2;
		array[2]=15;

		MetodosArrays.sumarNumeroAElementos(array, numero1);
		
		MetodosArrays.visualizarArray(array);	
		
		MetodosArrays.contarElementos(array);
		
		MetodosArrays.mostrarImpares(array);
		
		MetodosArrays.mayoresDeDiez(array);
		
		System.out.println("-----------------------------");
		System.out.println("Metodos de enteros y Strings.");
		System.out.println("------------------------------");
		
		Metodos.esPrimo(numero1);
		
		System.out.println("Cadena invertida: " + Metodos.invertirCadena(cadena));
		
		System.out.println("Cadena sin espacios: " + Metodos.omitirEspacios(cadena));
		
		System.out.println("Cuadrado de " + numero1 + ": " + Metodos.cuadrado(numero1));
		
		System.out.println("Division de " + numero1 + " entre 2: " + Metodos.dividir(numero1, 2));
		

	}

}
