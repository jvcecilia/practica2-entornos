﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[5];
            for (int i = 0; i < 5; i++) {
                a[i] = 8+i;
            }

            ClassLibrary1.MetodosArrays.visualizarArray(a);
            Console.WriteLine();
            ClassLibrary1.MetodosArrays.mostrarImpares(a);
            Console.WriteLine();
            ClassLibrary1.MetodosArrays.mayoresDeDiez(a);
            Console.WriteLine();
            ClassLibrary1.MetodosArrays.contarElementos(a);
            Console.WriteLine();
            ClassLibrary1.MetodosArrays.sumarNumeroAElementos(a, 5);
            Console.WriteLine();

            Console.WriteLine("Metodos de enteros y Strings.");
            Console.WriteLine();

            
            ClassLibrary1.Metodos.esPrimo(7);
            Console.WriteLine();
            Console.WriteLine("Resultado de la division: " + ClassLibrary1.Metodos.dividir(8, 2));
            Console.WriteLine();
            Console.WriteLine(ClassLibrary1.Metodos.invertirCadena("Cadena invertida"));
            Console.WriteLine();
            Console.WriteLine(ClassLibrary1.Metodos.omitirEspacios("Cadena sin espacios"));
            Console.WriteLine();
            Console.WriteLine("El cuadrado de 5 es: " + ClassLibrary1.Metodos.cuadrado(5));
            Console.ReadKey();

        }
    }
}
