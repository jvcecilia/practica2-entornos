﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Metodos
    {
        
        public static int dividir(int valor1, int valor2)
        {
            int resultado = valor1 / valor2;
            return resultado;
        }


        public static long cuadrado(long numero)
        {

            long cuadrado = numero*numero;

            return cuadrado;
        }
    

        public static Boolean esPrimo(int numero)
        {
            int contador = 0;

            for (int i = 1; i <= numero; i++)
            {
                if (numero % i == 0)
                {
                    contador++;
                }

            }
            if (contador == 2)
            {
                Console.WriteLine("El numero " + numero + " es primo!");
                return true;
            }
            Console.WriteLine("El numero " + numero + " no es primo.");
            return false;
        }

        public static String invertirCadena(String cadena)
        {

            String cadenaInvertida = "";

            for (int i = cadena.Length-1; i >= 0; i--)
            {
                cadenaInvertida += cadena[i];
            }

            return cadenaInvertida;
        }

        public static String omitirEspacios(String cadena)
        {
            String cadenaSin = "";

            for (int i = 0; i < cadena.Length; i++)
            {
                if (cadena[i] != ' ')
                {
                    cadenaSin += cadena[i];
                }
            }

            return cadenaSin;

        }
}
}
