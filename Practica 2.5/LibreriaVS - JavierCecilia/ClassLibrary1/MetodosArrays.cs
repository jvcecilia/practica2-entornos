﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class MetodosArrays
    {
        public static void visualizarArray(int[] array)
        {
            Console.WriteLine("------------------------------------");
            Console.WriteLine("Se muestran los elementos de array");
            Console.WriteLine("------------------------------------");
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("Posicion " + i + ": " + array[i]);
            }
        }

        public static void mayoresDeDiez(int[] array)
        {

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > 10)
                {
                    Console.WriteLine("Mayor de 10 en la posicion " + i + ": " + array[i]);
                }
            }
        }

        public static void mostrarImpares(int[] array)
        {

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] % 2 != 0)
                {
                    Console.WriteLine("Numero impar en la posicion " + i + ": " + array[i]);
                }
            }
        }
        public static void sumarNumeroAElementos(int[] array, int numero)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += numero;
            }
        }

        public static void contarElementos(int[] array)
        {
            int contador = 0;
            for (int i = 0; i < array.Length; i++)
            {
                contador++;
            }
            Console.WriteLine("Numero de elementos:" + contador);
        }
    }
}
