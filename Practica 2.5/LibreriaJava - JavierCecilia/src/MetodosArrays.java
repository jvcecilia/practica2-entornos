public class MetodosArrays {


	public static void visualizarArray(int[] array) {
		System.out.println("------------------------------------");
		System.out.println("Se muestran los elementos de array");
		System.out.println("------------------------------------");
		
		for (int i = 0; i < array.length; i++) {
			System.out.println("Posicion " + i + ": " + array[i]);
		}
	}

	
	public static void mayoresDeDiez(int[] array){
		
		for (int i = 0; i < array.length; i++) {
			if(array[i]>10){
			System.out.println("Mayor de 10 en la posici�n " + i + ": " + array[i]);
			}
		}	
	}
	
	public static void mostrarImpares(int[] array){
		for (int i = 0; i < array.length; i++) {
			if(array[i]%2!=0){
			System.out.println("Numero impar en la posicion " + i + ": " + array[i]);
			}
		}
	}
	
	public static void sumarNumeroAElementos(int[] array,int numero){
		
		for (int i = 0; i < array.length; i++) {
			array[i] += numero;
		}
		
	}
	
	public static void contarElementos(int[] array){
		
		int contador = 0;
		for (int i = 0; i < array.length; i++) {
			contador++;
		}
		
		System.out.println("Numero de elementos:" + contador);
	}
}