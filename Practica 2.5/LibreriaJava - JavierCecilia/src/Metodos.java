
public class Metodos {
	
	public static boolean esPrimo(int numero){
		int contador=0;
		System.out.println();
		
		for (int i = 1; i <= numero; i++) {
			if(numero%i==0){
				contador++;
			}
			}
		
		if(contador==2){
			System.out.println("El numero " + numero  + " es primo!");
			return true;
		}
		System.out.println("El numero " + numero  + " no es primo.");
		return false;
	}

	public static long cuadrado (int numero){
		
		long cuadrado = numero*numero;
		
		return cuadrado;
	}
	
	public static String invertirCadena(String cadena){
		
		String cadenaInvertida = "";
		
		for (int i = cadena.length()-1; i >=0 ; i--) {
			cadenaInvertida += cadena.charAt(i); 
		}
		
		return cadenaInvertida;
	}
	
	public static int dividir(int valor1, int valor2){
		
		int resultado = valor1/valor2;
		
		return resultado;
	}
	
	public static String omitirEspacios(String cadena){
		
		String cadenaSin = "";
		
		for (int i = 0; i < cadena.length(); i++) {
			if(cadena.charAt(i) != ' '){
				cadenaSin += cadena.charAt(i);
			}
		}
		
		return cadenaSin;
		
	}
}
