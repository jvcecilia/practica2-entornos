package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){

		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		/*El problema del ejercicio es que en el primer scanner, al introducir un numero entero estas tambi�n �enviando� un enter, 
		 * el cual, al no poder almacenarse en la variable que solo admite enteros, se introducira en la primera variable que pille.
		 * */
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		// Una soluci�n a este problema es almacenar ese espacio en blanco en un .nextline que no este unido a ninguna varaible, 
		// perdiendo este espacio y dejando el buffer libre para las siguientes cadenas.
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
