package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 *  
		 */
		
		
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		/*El problema del ejercicio es que, al incluir en el bucle que vaya hasta la igualdad de la cadena inizializando este a 0, siempre va a haber
		un caracter mas de lo que escribas. Ejemplo: "Hola" tiene 4 caracteres, pero al ir desde el 0 hasta el 4, hay 5.*/

		/*Hay dos formas de solucionar esto, modificando la inizializacion del bucle a i=1 
		 * o bien quitando la igualdad a la segunda parte del bucle (i<cadena.lenght())
		 */
		for(int i = 0 ; i <= cadenaLeida.length(); i++){
			caracter = cadenaLeida.charAt(i);
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
