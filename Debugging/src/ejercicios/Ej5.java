package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		/* La funci�n del programa es decirte si un numero introducido por teclado es primo o no.
		 * El funcionamiento de este es: Mediante un bucle, te reproduce los numeros desde el 1 hasta el numero introducido, y dentro de este,
		 * si el numero es divisible entre alguno de sus anteriores te suma un numero en el contador.
		 * Al final del bucle, si ha habido mas de dos numeros divisibles significa que hay mas numeros a parte de la unidad y el mismo divisibles del
		 * numero introducido por teclado, por lo que el numero no ser�a primo. En caso de que no los hubiese, el numero es primo.
		 * */
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
