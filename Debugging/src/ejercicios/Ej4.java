package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		/*El error de este ejercicio es que, en la segunda parte del bucle, esta incluida la posibilidad de "=0", por lo que al llegar a i=0,
		 el ejercicio da error, ya que no hay ning�n numero divisible por 0.
		 La soluci�n a este ejercicio es quitar la igualdad en el segundo parametro del bucle, para que cuando i valga 0 no vuelva a entrar en el bucle. 
		*/
		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}

}
