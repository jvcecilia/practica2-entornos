package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		/*El problema de este ejercicio es que el numero que obtiene la varaible char representa el bot�n �Escape� del ordenador, 
		 * el cual no es ning�n car�cter que se pueda almacenar en un string, por lo cual, al llegar al substring siempre va a dar error.
		 * */
		
		/* Una soluci�n para que este ejercicio tuviese sentido ser�a dar la posibilidad de introducir el char que tu quieras (Con un scanner) 
		 * y as� tener libertad de leer las cadenas de la forma que tu quieras.
		 *  */
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
