package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.ImageIcon;
import java.awt.FlowLayout;
import java.awt.Color;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.JSpinner;
import javax.swing.JCheckBox;
import javax.swing.JTabbedPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

/**
 * 
 * @author Javier Cecilia
 * @since 21-12-2017
 * 
 *
 */
public class VentanaPersonalizada extends JFrame {

	private JPanel contentPane;
	private JTextField txtVolumenMsica;
	private JTextField txtPokmon;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPersonalizada frame = new VentanaPersonalizada();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPersonalizada() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaPersonalizada.class.getResource("/Imagenes/hqdefault.jpg")));
		setTitle("AnimeLand");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 453, 352);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Series");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Acci\u00F3n");
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Drama");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Romance");
		mnNewMenu.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Misterio");
		mnNewMenu.add(mntmNewMenuItem_3);
		
		JMenu mnNewMenu_1 = new JMenu("Foros");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Noticias");
		mnNewMenu_1.add(mntmNewMenuItem_4);
		
		JMenuItem mntmNewMenuItem_5 = new JMenuItem("Debates");
		mnNewMenu_1.add(mntmNewMenuItem_5);
		
		JMenuItem mntmNewMenuItem_6 = new JMenuItem("Animacion");
		mnNewMenu_1.add(mntmNewMenuItem_6);
		
		JMenu mnNewMenu_2 = new JMenu("Ayuda");
		menuBar.add(mnNewMenu_2);
		contentPane = new JPanel();
		contentPane.setBackground(Color.CYAN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JSlider slider = new JSlider();
		slider.setBounds(237, 270, 200, 23);
		contentPane.add(slider);
		
		txtVolumenMsica = new JTextField();
		txtVolumenMsica.setBackground(Color.PINK);
		txtVolumenMsica.setText(" Volumen M\u00FAsica");
		txtVolumenMsica.setBounds(289, 250, 103, 20);
		contentPane.add(txtVolumenMsica);
		txtVolumenMsica.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(VentanaPersonalizada.class.getResource("/Imagenes/ea28c359f6d1a64aa5815ca00c9558b2--pixel-pokemon-pixel-art.jpg")));
		lblNewLabel.setBounds(0, 30, 207, 158);
		contentPane.add(lblNewLabel);
		
		txtPokmon = new JTextField();
		txtPokmon.setBackground(Color.PINK);
		txtPokmon.setText("Pok\u00E9mon");
		txtPokmon.setBounds(62, 11, 63, 20);
		contentPane.add(txtPokmon);
		txtPokmon.setColumns(10);
		
		JButton btnSiguienteAnime = new JButton("Siguiente anime");
		btnSiguienteAnime.setBounds(37, 188, 139, 23);
		contentPane.add(btnSiguienteAnime);
		
		JButton btnNewButton = new JButton("Anterior anime");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(37, 211, 139, 23);
		contentPane.add(btnNewButton);
		
		JCheckBox chckbxFavorito = new JCheckBox("Favorito");
		chckbxFavorito.setBackground(Color.MAGENTA);
		chckbxFavorito.setBounds(207, 165, 97, 23);
		contentPane.add(chckbxFavorito);
		
		JRadioButton rdbtnTemporada = new JRadioButton("1 Temporada");
		rdbtnTemporada.setBackground(Color.ORANGE);
		buttonGroup.add(rdbtnTemporada);
		rdbtnTemporada.setBounds(213, 54, 109, 23);
		contentPane.add(rdbtnTemporada);
		
		JRadioButton rdbtnTemporada_1 = new JRadioButton("2 Temporada");
		rdbtnTemporada_1.setBackground(Color.ORANGE);
		buttonGroup.add(rdbtnTemporada_1);
		rdbtnTemporada_1.setBounds(213, 80, 109, 23);
		contentPane.add(rdbtnTemporada_1);
		
		JRadioButton rdbtnTemporada_2 = new JRadioButton("3 Temporada");
		rdbtnTemporada_2.setBackground(Color.ORANGE);
		buttonGroup.add(rdbtnTemporada_2);
		rdbtnTemporada_2.setBounds(213, 106, 109, 23);
		contentPane.add(rdbtnTemporada_2);
	}
}
