﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.librosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.romanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.misterioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cienciaFiccionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sigloXXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sigloXXIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hOlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xDToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.romanceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dramaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.misterioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fechaDeSalidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sigloXXIToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.autoresToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.epocaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eventsoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quienesSomosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ubicaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contactaConNosotrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.x = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.librefestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otakuWorkBoockFestivalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.linkImaginarioDeWikipediacomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cCoso25ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cLucasDeCordobaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ayudaToolStripMenuItem,
            this.librosToolStripMenuItem,
            this.autoresToolStripMenuItem,
            this.historiaToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(116, 92);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // librosToolStripMenuItem
            // 
            this.librosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.romanceToolStripMenuItem,
            this.misterioToolStripMenuItem,
            this.cienciaFiccionToolStripMenuItem});
            this.librosToolStripMenuItem.Name = "librosToolStripMenuItem";
            this.librosToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.librosToolStripMenuItem.Text = "Libros";
            // 
            // romanceToolStripMenuItem
            // 
            this.romanceToolStripMenuItem.Name = "romanceToolStripMenuItem";
            this.romanceToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.romanceToolStripMenuItem.Text = "Romance";
            // 
            // misterioToolStripMenuItem
            // 
            this.misterioToolStripMenuItem.Name = "misterioToolStripMenuItem";
            this.misterioToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.misterioToolStripMenuItem.Text = "Misterio";
            // 
            // cienciaFiccionToolStripMenuItem
            // 
            this.cienciaFiccionToolStripMenuItem.Name = "cienciaFiccionToolStripMenuItem";
            this.cienciaFiccionToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.cienciaFiccionToolStripMenuItem.Text = "Ciencia Ficcion";
            // 
            // autoresToolStripMenuItem
            // 
            this.autoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sigloXXToolStripMenuItem,
            this.sigloXXIToolStripMenuItem});
            this.autoresToolStripMenuItem.Name = "autoresToolStripMenuItem";
            this.autoresToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.autoresToolStripMenuItem.Text = "Autores";
            // 
            // sigloXXToolStripMenuItem
            // 
            this.sigloXXToolStripMenuItem.Name = "sigloXXToolStripMenuItem";
            this.sigloXXToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.sigloXXToolStripMenuItem.Text = "Siglo XX";
            // 
            // sigloXXIToolStripMenuItem
            // 
            this.sigloXXIToolStripMenuItem.Name = "sigloXXIToolStripMenuItem";
            this.sigloXXIToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.sigloXXIToolStripMenuItem.Text = "Siglo XXI";
            // 
            // historiaToolStripMenuItem
            // 
            this.historiaToolStripMenuItem.Name = "historiaToolStripMenuItem";
            this.historiaToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.historiaToolStripMenuItem.Text = "Historia";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hOlaToolStripMenuItem,
            this.aToolStripMenuItem,
            this.ayudaToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(366, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // hOlaToolStripMenuItem
            // 
            this.hOlaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xDToolStripMenuItem,
            this.eventsoToolStripMenuItem});
            this.hOlaToolStripMenuItem.Name = "hOlaToolStripMenuItem";
            this.hOlaToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.hOlaToolStripMenuItem.Text = "General";
            this.hOlaToolStripMenuItem.Click += new System.EventHandler(this.hOlaToolStripMenuItem_Click);
            // 
            // xDToolStripMenuItem
            // 
            this.xDToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xDToolStripMenuItem1,
            this.fechaDeSalidaToolStripMenuItem,
            this.autoresToolStripMenuItem1});
            this.xDToolStripMenuItem.Name = "xDToolStripMenuItem";
            this.xDToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.xDToolStripMenuItem.Text = "Libros";
            this.xDToolStripMenuItem.Click += new System.EventHandler(this.xDToolStripMenuItem_Click);
            // 
            // xDToolStripMenuItem1
            // 
            this.xDToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.romanceToolStripMenuItem1,
            this.dramaToolStripMenuItem,
            this.misterioToolStripMenuItem1});
            this.xDToolStripMenuItem1.Name = "xDToolStripMenuItem1";
            this.xDToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
            this.xDToolStripMenuItem1.Text = "Categoria";
            this.xDToolStripMenuItem1.Click += new System.EventHandler(this.xDToolStripMenuItem1_Click);
            // 
            // romanceToolStripMenuItem1
            // 
            this.romanceToolStripMenuItem1.Name = "romanceToolStripMenuItem1";
            this.romanceToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.romanceToolStripMenuItem1.Text = "Romance";
            // 
            // dramaToolStripMenuItem
            // 
            this.dramaToolStripMenuItem.Name = "dramaToolStripMenuItem";
            this.dramaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.dramaToolStripMenuItem.Text = "Drama";
            // 
            // misterioToolStripMenuItem1
            // 
            this.misterioToolStripMenuItem1.Name = "misterioToolStripMenuItem1";
            this.misterioToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.misterioToolStripMenuItem1.Text = "Misterio";
            // 
            // fechaDeSalidaToolStripMenuItem
            // 
            this.fechaDeSalidaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sigloXXIToolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.fechaDeSalidaToolStripMenuItem.Name = "fechaDeSalidaToolStripMenuItem";
            this.fechaDeSalidaToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.fechaDeSalidaToolStripMenuItem.Text = "Fecha de salida";
            // 
            // sigloXXIToolStripMenuItem1
            // 
            this.sigloXXIToolStripMenuItem1.Name = "sigloXXIToolStripMenuItem1";
            this.sigloXXIToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.sigloXXIToolStripMenuItem1.Text = "2018";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem2.Text = "2017";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem3.Text = "2016";
            // 
            // autoresToolStripMenuItem1
            // 
            this.autoresToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.epocaToolStripMenuItem});
            this.autoresToolStripMenuItem1.Name = "autoresToolStripMenuItem1";
            this.autoresToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
            this.autoresToolStripMenuItem1.Text = "Autores";
            // 
            // epocaToolStripMenuItem
            // 
            this.epocaToolStripMenuItem.Name = "epocaToolStripMenuItem";
            this.epocaToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.epocaToolStripMenuItem.Text = "Epoca";
            // 
            // eventsoToolStripMenuItem
            // 
            this.eventsoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.librefestToolStripMenuItem,
            this.otakuWorkBoockFestivalToolStripMenuItem});
            this.eventsoToolStripMenuItem.Name = "eventsoToolStripMenuItem";
            this.eventsoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.eventsoToolStripMenuItem.Text = "Eventos";
            this.eventsoToolStripMenuItem.Click += new System.EventHandler(this.eventsoToolStripMenuItem_Click);
            // 
            // aToolStripMenuItem
            // 
            this.aToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quienesSomosToolStripMenuItem,
            this.ubicaciónToolStripMenuItem,
            this.contactaConNosotrosToolStripMenuItem});
            this.aToolStripMenuItem.Name = "aToolStripMenuItem";
            this.aToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.aToolStripMenuItem.Text = "Información";
            // 
            // quienesSomosToolStripMenuItem
            // 
            this.quienesSomosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.linkImaginarioDeWikipediacomToolStripMenuItem});
            this.quienesSomosToolStripMenuItem.Name = "quienesSomosToolStripMenuItem";
            this.quienesSomosToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.quienesSomosToolStripMenuItem.Text = "¿Quienes somos?";
            // 
            // ubicaciónToolStripMenuItem
            // 
            this.ubicaciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cCoso25ToolStripMenuItem,
            this.cLucasDeCordobaToolStripMenuItem});
            this.ubicaciónToolStripMenuItem.Name = "ubicaciónToolStripMenuItem";
            this.ubicaciónToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.ubicaciónToolStripMenuItem.Text = "Ubicación Librerias";
            // 
            // contactaConNosotrosToolStripMenuItem
            // 
            this.contactaConNosotrosToolStripMenuItem.Name = "contactaConNosotrosToolStripMenuItem";
            this.contactaConNosotrosToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.contactaConNosotrosToolStripMenuItem.Text = "Contacta con nosotros";
            // 
            // ayudaToolStripMenuItem1
            // 
            this.ayudaToolStripMenuItem1.Name = "ayudaToolStripMenuItem1";
            this.ayudaToolStripMenuItem1.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem1.Text = "Ayuda";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Blue;
            this.button1.Location = new System.Drawing.Point(139, 217);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 24);
            this.button1.TabIndex = 3;
            this.button1.Text = "Registrar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(71, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Usted ha decidido registrarse en nuestra web.";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "Test 1";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(233, 126);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 5;
            this.comboBox1.Text = "1995";
            this.comboBox1.ValueMember = "Test2";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.radioButton1.Location = new System.Drawing.Point(120, 154);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(62, 17);
            this.radioButton1.TabIndex = 6;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Hombre";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // x
            // 
            this.x.AutoSize = true;
            this.x.Location = new System.Drawing.Point(233, 184);
            this.x.Name = "x";
            this.x.Size = new System.Drawing.Size(15, 14);
            this.x.TabIndex = 9;
            this.x.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(39, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(298, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "A continuación debera rellanar una serie de datos personales:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(70, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Nombre usuario";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(70, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Año nacimiento";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(70, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Quiere recibir notificaciones?";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.radioButton2.Location = new System.Drawing.Point(197, 154);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(51, 17);
            this.radioButton2.TabIndex = 15;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Mujer";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(233, 77);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 16;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(70, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Contraseña";
            this.label6.Click += new System.EventHandler(this.label6_Click_1);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(233, 99);
            this.textBox2.Name = "textBox2";
            this.textBox2.PasswordChar = '*';
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 18;
            // 
            // librefestToolStripMenuItem
            // 
            this.librefestToolStripMenuItem.Name = "librefestToolStripMenuItem";
            this.librefestToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.librefestToolStripMenuItem.Text = "Librefest";
            // 
            // otakuWorkBoockFestivalToolStripMenuItem
            // 
            this.otakuWorkBoockFestivalToolStripMenuItem.Name = "otakuWorkBoockFestivalToolStripMenuItem";
            this.otakuWorkBoockFestivalToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.otakuWorkBoockFestivalToolStripMenuItem.Text = "OtakuWorkBookFestival";
            // 
            // linkImaginarioDeWikipediacomToolStripMenuItem
            // 
            this.linkImaginarioDeWikipediacomToolStripMenuItem.Name = "linkImaginarioDeWikipediacomToolStripMenuItem";
            this.linkImaginarioDeWikipediacomToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.linkImaginarioDeWikipediacomToolStripMenuItem.Text = "LinkImaginarioDeWikipedia.com";
            // 
            // cCoso25ToolStripMenuItem
            // 
            this.cCoso25ToolStripMenuItem.Name = "cCoso25ToolStripMenuItem";
            this.cCoso25ToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.cCoso25ToolStripMenuItem.Text = "C/ Coso 25";
            // 
            // cLucasDeCordobaToolStripMenuItem
            // 
            this.cLucasDeCordobaToolStripMenuItem.Name = "cLucasDeCordobaToolStripMenuItem";
            this.cLucasDeCordobaToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.cLucasDeCordobaToolStripMenuItem.Text = "C/ Lucas de cordoba";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.descarga;
            this.ClientSize = new System.Drawing.Size(366, 262);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.x);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.Cursor = System.Windows.Forms.Cursors.No;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Registro Libreria Super Mario 64";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contextMenuStrip2.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem librosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem romanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem misterioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cienciaFiccionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sigloXXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sigloXXIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hOlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xDToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem romanceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dramaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem misterioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fechaDeSalidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sigloXXIToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem eventsoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quienesSomosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ubicaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contactaConNosotrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoresToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem epocaToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox x;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ToolStripMenuItem librefestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otakuWorkBoockFestivalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem linkImaginarioDeWikipediacomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cCoso25ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cLucasDeCordobaToolStripMenuItem;
    }
}

